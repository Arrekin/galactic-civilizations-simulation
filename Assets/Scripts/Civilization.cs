﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

public class Civilization
{
    public Color Color { get; set; }

    public AttitudeType Attitude { get; private set; }

    public List<SolarSystem> SolarSystems { get; private set; }

    public int SpaceTravelTechnology { get; private set; }

    public int StepsTillNextExpansion { get; private set; }

    public int ExpansionPower { get; private set; }

    public float ResearchModifier { get; private set; }

    public float MilitaryBonus { get; private set; }

    public List<Civilization> War { get; private set; } 

    public Civilization(Color color)
    {
        SolarSystems = new List<SolarSystem>();
        Color = color;
        Attitude = (AttitudeType)Random.Range(0, 3);
        switch (Attitude)
        {
            case AttitudeType.Agressive:
                ExpansionPower = 2;
                ResearchModifier = 0.8f;
                MilitaryBonus = 1.2f;
                break;
            case AttitudeType.Neutral:
                ExpansionPower = 1;
                ResearchModifier = 1f;
                MilitaryBonus = 1.2f;
                break;
            case AttitudeType.Peaceful:
                ExpansionPower = 2;
                ResearchModifier = 1.2f;
                MilitaryBonus = 0.8f;
                break;
        }
        StepsTillNextExpansion = -1;
        SpaceTravelTechnology = 0;
        War = new List<Civilization>();
    }

    public void AddSolarSystem(SolarSystem ssToAdd)
    {
        if( !SolarSystems.Contains(ssToAdd))
            SolarSystems.Add(ssToAdd);
        ssToAdd.civilization = this;
        ssToAdd.OwnerChanged();
    }

    public void RemoveFromSolarSystem(SolarSystem ssToRemove)
    {
        SolarSystems.RemoveAll(ss => ss == ssToRemove);
        ssToRemove.civilization = null;
        ssToRemove.OwnerChanged();

        if( SolarSystems.Count==0 )
            SimulationManager.Self.CivilizationsList.Remove(this);
    }

    public void Step()
    {
        if (SpaceTravelTechnology != 0)
        {
            if (StepsTillNextExpansion <= 0)
            {
                if (StepsTillNextExpansion != -1)
                {
                    Expansion();
                }
                StepsTillNextExpansion = 11 - SpaceTravelTechnology;
            }
            --StepsTillNextExpansion;
        }

        if (SpaceTravelTechnology < 9 && Random.value < (0.1f-0.01f*SpaceTravelTechnology)*ResearchModifier)
        {
            ++SpaceTravelTechnology;
            if (StepsTillNextExpansion > 1)
                --StepsTillNextExpansion;
        }
    }

    private void Expansion()
    {
        var expPow = ExpansionPower;
        var tx = SimulationManager.tx;
        var tz = SimulationManager.tz;
        Queue<SolarSystem> q = new Queue<SolarSystem>();
        foreach (var ss in SolarSystems)
        {
            for (var k = 0; k < 8; ++k)
            {
                int nx = ss.PositionX + tx[k];
                int nz = ss.PositionZ + tz[k];
                if (SimulationManager.Self.Validate(nx, nz))
                {
                    var nss = SimulationManager.Self.galaxy[nx, nz];
                    if (nss.HasStar && nss.civilization!=this && Random.Range(0.5f*nss.lifeSupport, 1f) > 0.98f)
                    {
                        if (Attitude == AttitudeType.Neutral)
                        {
                            if (nss.LowerCivilizationExists || (nss.civilization != null && !War.Contains(nss.civilization)))
                                continue;
                        }
                        if (Attitude == AttitudeType.Peaceful)
                        {
                            if (nss.civilization != null && !War.Contains(nss.civilization))
                                continue;
                        }
                        --expPow;
                        q.Enqueue(nss);
                    }
                }
            }
            if (expPow == 0) break;
        }
        while (q.Count != 0)
        {
            var nss = q.Dequeue();
            if (nss.civilization != null)
            {
                if( !nss.civilization.War.Contains(this))
                    nss.civilization.War.Add(this);

                if (Random.value*MilitaryBonus < Random.value*nss.civilization.MilitaryBonus)
                    continue; // invasion failed
                nss.civilization.RemoveFromSolarSystem(nss);
            }
            nss.LowerCivilizationExists = false;
            AddSolarSystem(nss);
        }
    }

    public enum AttitudeType
    {
        Agressive,
        Neutral,
        Peaceful,
    }
}