﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SimulationManager : MonoBehaviour
{
    public int galaxyRadius = 20;
    public float timePerStep = 1;
    public float gasEscapeRatio = 0.01f;
    public float starsFormationRatio = 0.001f;
    public float minimumGasForStarFormation = 10000f;
    public float starGasConsumption = 0.1f;
    public GameObject solarSystemPrefab;
    public Transform solarSystemsLabel;

    public int Step { get; private set; }
    public static SimulationManager Self { get; private set; }

    public List<Civilization> CivilizationsList = new List<Civilization>(); 

    public SolarSystem[,] galaxy;
    private float counter;

    public static readonly int[] tx = { 1, -1, 0, 0, 1, -1, 1, -1 };
    public static readonly int[] tz = { 0, 0, 1, -1, 1, -1, -1, 1 };

    private Queue<SolarSystem> q = new Queue<SolarSystem>();

    [Serializable]
    public class UiElements
    {
        public Text StepText;
    }

    public UiElements uiElements;

    public SolarSystem.IndicatorType Indicator = SolarSystem.IndicatorType.Noone;


    public void StarDeath(SolarSystem ss)
    {
        var x = ss.PositionX;
        var z = ss.PositionZ;
        var classtype = ss.StarClass;

        var elementsInSystem = ss.heavyElements;
        var elementsForged = 0;
        var additionalElementsFroged = 0;
        switch (classtype)
        {
            case SolarSystem.StarClassType.O:
                elementsForged = 100;
                additionalElementsFroged = 20;
                break;
            case SolarSystem.StarClassType.B:
                elementsForged = 90;
                additionalElementsFroged = 18;
                break;
            case SolarSystem.StarClassType.A:
                elementsForged = 85;
                additionalElementsFroged = 16;
                break;
            case SolarSystem.StarClassType.F:
                elementsForged = 80;
                additionalElementsFroged = 12;
                break;
            case SolarSystem.StarClassType.G:
                elementsForged = 40;
                additionalElementsFroged = 8;
                break;
            case SolarSystem.StarClassType.K:
                elementsForged = 30;
                additionalElementsFroged = 5;
                break;
            case SolarSystem.StarClassType.M:
                elementsForged = 20;
                additionalElementsFroged = 2;
                break;

        }
        q.Clear();
        for (var k = 0; k < 8; ++k)
        {
            int nx = x + tx[k];
            int nz = z + tz[k];
            if (Validate(nx, nz))
                q.Enqueue(galaxy[nx, nz]);
        }
        var numf = q.Count; // number of valid fields
        if (numf > 0)
        {
            var change = 0.3f * elementsInSystem; // total amount of heavy elements in the system
            ss.heavyElements -= change; // substraction of escaped gas
            change /= numf; // number of gas that will arrive to every field
            while (q.Count > 0)
            {
                var elem = q.Dequeue();
                elem.heavyElements += change+additionalElementsFroged;
                elem.HeavyElementsChanged();
            }
        }
        ss.heavyElements += elementsForged;
        ss.HeavyElementsChanged();
        ss.Supernova();
    }

    public bool Validate(int x, int z)
    {
        return x >= 0 && x < galaxyRadius * 2 && z >= 0 && z < galaxyRadius * 2;
    }

    // Use this for initialization
    private void Start ()
	{
	    Self = this;
	    Step = 1;
        galaxy = new SolarSystem[galaxyRadius*2,galaxyRadius*2];
        for( var i = 0; i < galaxyRadius*2; ++i)
            for (var j = 0; j < galaxyRadius*2; ++j)
            {
                var ss = Instantiate(solarSystemPrefab).GetComponent<SolarSystem>();
                ss.transform.position = new Vector3(i*100,Random.Range(-200,200),j*100);
                ss.transform.SetParent(solarSystemsLabel);
                ss.name = "SolarSystem[" + i + "," + j + "]";
                ss.SetPosition(i,j);
                galaxy[i, j] = ss;
            }
	}
	
	// Update is called once per frame
	private void Update ()
	{
	    counter += Time.deltaTime;
	    if (counter > timePerStep)
	    {
	        SimulationStep();
	        counter = 0;
	    }
	}

    private void SimulationStep()
    {
        GasMigration();
        CivilizationsAndStarsForming();

        CivilizationsActions();

        ApplyStep();


        ++Step;
        if (uiElements.StepText != null)
            uiElements.StepText.text = Step.ToString();
    }

    private void GasMigration()
    {
        for (var i = 0; i < galaxyRadius*2; ++i)
            for (var j = 0; j < galaxyRadius*2; ++j)
            {
                var dist = Mathf.Abs(i - 20) + Mathf.Abs(j - 20);
                q.Clear();
                for (var k = 0; k < 8; ++k)
                {
                    int nx = i + tx[k];
                    int nz = j + tz[k];
                    if (!Validate(nx, nz)) continue;
                    if (Mathf.Abs(nx - 20) + Mathf.Abs(nz - 20) <= dist && galaxy[nx,nz].CanAcceptGas)
                        q.Enqueue(galaxy[nx,nz]);
                }
                var numf = q.Count; // number of escape fields
                if (numf > 0)
                {
                    var change = gasEscapeRatio*galaxy[i, j].gas; // total amount of gas which escaped
                    galaxy[i, j].futureGas -= change; // substraction of escaped gas
                    change /= numf; // number of gas that will arrive to every field
                    while (q.Count > 0)
                    {
                        var elem = q.Dequeue();
                        elem.futureGas += change;
                    }
                }
            }
    }

    private void CivilizationsAndStarsForming()
    {
        for (var i = 0; i < galaxyRadius*2; ++i)
            for (var j = 0; j < galaxyRadius*2; ++j)
            {
                var ss = galaxy[i, j];
                // Lower Civ forming
                if (!ss.LowerCivilizationExists && ss.civilization == null && ss.lifeSupport>=0.2f)
                {
                    if (Random.value > 0.999f && Random.value < ss.lifeSupport)
                    {
                        ss.LowerCivilizationExists = true;
                        ss.LowerCivilizationChanged();
                    }
                }
                else if (ss.LowerCivilizationExists)
                {
                    if (Random.value > 0.995)
                    {
                        ss.LowerCivilizationExists = false;
                        ss.LowerCivilizationChanged();

                        var civCol = AssignNewCivilizationColor();
                        var newCiv = new Civilization(civCol);
                        newCiv.AddSolarSystem(ss);
                        CivilizationsList.Add(newCiv);
                    }
                }

                if (ss.HasStar || ss.gas < minimumGasForStarFormation /*&& Random.value> starsFormationRatio*starsFormationRatio*/) continue;
                var chance = Random.value;
                if (chance < starsFormationRatio + Math.Abs((ss.gas-minimumGasForStarFormation)/ (SolarSystem.MaxGasAmount*100)) )
                {
                    // gas usage
                    var starGas = Random.Range(0.2f,0.4f)*ss.gas; // star taking 20-40% of gas in the system
                    ss.futureGas -= starGas;
                    // now push 50% of gas to neighbor system
                    q.Clear();
                    for (var k = 0; k < 8; ++k)
                    {
                        int nx = i + tx[k];
                        int nz = j + tz[k];
                        if (Validate(nx, nz))
                            q.Enqueue(galaxy[nx, nz]);
                    }
                    var numf = q.Count; // number of valid fields
                    if (numf > 0)
                    {
                        var change = 0.5f * ss.gas; // total amount of gas which escaped
                        ss.futureGas -= change; // substraction of escaped gas
                        change /= numf; // number of gas that will arrive to every field
                        while (q.Count > 0)
                        {
                            var elem = q.Dequeue();
                            elem.futureGas += change;
                        }
                    }
                    //star creation
                    ss.Ignite(starGas);
                }   
            }
    }

    private void CivilizationsActions()
    {
        foreach (var civilization in CivilizationsList)
        {
            civilization.Step();
        }
    }

    private Color AssignNewCivilizationColor()
    {
        Color col;
        while (true)
        {
            col = new Color(Random.value, Random.value, Random.value, 0.7f);
            bool ok = CivilizationsList.Select(civ => Math.Abs(col.r - civ.Color.r) + Math.Abs(col.g - civ.Color.g) + Math.Abs(col.b - civ.Color.b)).All(val => !(val < 0.2f));
            if (ok) break;
        }
        return col;
    }

    private void ApplyStep()
    {
        for (var i = 0; i < galaxyRadius*2; ++i)
            for (var j = 0; j < galaxyRadius*2; ++j)
            {
                galaxy[i, j].Step();
            }
    }
}
