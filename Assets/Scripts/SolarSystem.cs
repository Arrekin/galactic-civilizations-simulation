﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions.Must;

public class SolarSystem : MonoBehaviour
{
    // current step value
    public float gas;
    // future step value
    public float futureGas;
    // gas number that is sun made of
    public float starGas;
    // number of heavy elements
    public float heavyElements;
    // how good is life support in the system
    public float lifeSupport;
    // System owner
    public Civilization civilization;

    public GameObject indicatorCube;
    public GameObject star;

    public bool HasStar { get; private set; }
    public StarClassType StarClass { get; private set; }
    public int StarLifePoints { get; private set; }

    public bool LowerCivilizationExists { get; set; }

    public bool CanAcceptGas { get { return futureGas < MaxGasAmount && !HasStar; } }

    public int PositionX { get; private set; }
    public int PositionZ { get; private set; }

    public static float MaxGasAmount
    {
        get { return 50000f; }
    }

    public static float MaxHeavyElementsAmount
    {
        get { return 10000f; }
    }

    private IndicatorType indicatorType = IndicatorType.Noone;

    public void SetPosition(int x, int z)
    {
        PositionX = x;
        PositionZ = z;
    }

    public void Step()
    {
        SetGas(futureGas);

        if (HasStar)
            if (--StarLifePoints == 0)
                SimulationManager.Self.StarDeath(this);
    }

    public void HeavyElementsChanged()
    {
        if (heavyElements > MaxHeavyElementsAmount)
            heavyElements = MaxHeavyElementsAmount;
        if( indicatorType == IndicatorType.HeavyElements )
            indicatorCube.GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1, heavyElements / MaxHeavyElementsAmount);
    }

    public void LifeSupportChanged()
    {
        if( indicatorType == IndicatorType.LifeSupport )
            indicatorCube.GetComponent<MeshRenderer>().material.color = new Color(0, 1, 0, lifeSupport);
    }

    public void LowerCivilizationChanged()
    {
        if( indicatorType == IndicatorType.LowerCivilizations )
            indicatorCube.GetComponent<MeshRenderer>().material.color = new Color(0, 0, 0, LowerCivilizationExists? 0.8f : 0f);
    }

    public void OwnerChanged()
    {
        if (indicatorType == IndicatorType.Civilizations)
            indicatorCube.GetComponent<MeshRenderer>().material.color = civilization==null? new Color(1,1,1,0) : civilization.Color;
    }

    /// <summary>
    /// Ignites the gas and forms a star
    /// </summary>
    public void Ignite(float gasForStar)
    {
        starGas = gasForStar;
        if (starGas > 0.36f*MaxGasAmount)
        {
            StarClass = StarClassType.O;
            StarLifePoints = Random.Range(80, 120);
        }
        else if (starGas > 0.32f*MaxGasAmount)
        {
            StarClass = StarClassType.B;
            StarLifePoints = Random.Range(140, 180);
        }
        else if (starGas > 0.28f*MaxGasAmount)
        { 
            StarClass = StarClassType.A;
            StarLifePoints = Random.Range(200, 250);
        }
        else if (starGas > 0.24f*MaxGasAmount)
        { 
            StarClass = StarClassType.F;
            StarLifePoints = Random.Range(260, 330);
        }
        else if (starGas > 0.20f*MaxGasAmount)
        { 
            StarClass = StarClassType.G;
            StarLifePoints = Random.Range(340, 400);
        }
        else if (starGas > 0.16f*MaxGasAmount)
        { 
            StarClass = StarClassType.K;
            StarLifePoints = Random.Range(420, 550);
        }
        else
        { 
            StarClass = StarClassType.M;
            StarLifePoints = Random.Range(600, 1000);
        }
        StarUpdate();
        // Now set life support level
        var lsv = Random.Range(0f, heavyElements/MaxHeavyElementsAmount);
        lifeSupport = heavyElements>0.1f*MaxHeavyElementsAmount ? Random.Range(0f, 1f) : lsv;
        LifeSupportChanged();
        star.SetActive(true);
        HasStar = true;

    }

    public void Supernova()
    {
        if (!HasStar) return;
        futureGas += starGas*Mathf.Max(0, 1f - SimulationManager.Self.starGasConsumption);
        lifeSupport = 0;
        LifeSupportChanged();
        LowerCivilizationExists = false;
        LowerCivilizationChanged();

        if( civilization!=null)
            civilization.RemoveFromSolarSystem(this);

        star.SetActive(false);
        HasStar = false;
        StarClass = StarClassType.Noone;

    }

    private void SetGas(float gasToSet)
    {
        gas = gasToSet;
        if (gas < 0) gas = 0;
        //if (gas > maxGasAmount) gas = maxGasAmount;
        futureGas = gas;
        if( indicatorType == IndicatorType.Gas)
            indicatorCube.GetComponent<MeshRenderer>().material.color = new Color(0,0,1,gas/MaxGasAmount);
    }

	// Use this for initialization
	void Start ()
	{
	    HasStar = false;
	    LowerCivilizationExists = false;
        StarClass = StarClassType.Noone;
	    SetGas(Random.Range(5000, 50000));
	}
	
	// Update is called once per frame
	void Update ()
	{
	    var sm = SimulationManager.Self;
	    if (indicatorType != sm.Indicator)
	    {
	        indicatorType = sm.Indicator;
	        switch (indicatorType)
	        {
	            case IndicatorType.Noone:
                    indicatorCube.SetActive(false);
	                break;
                case IndicatorType.Gas:
                    SetGas(gas);
                    indicatorCube.SetActive(true);
	                break;
                case IndicatorType.HeavyElements:
                    HeavyElementsChanged();
                    indicatorCube.SetActive(true);
	                break;
                case IndicatorType.LifeSupport:
                    LifeSupportChanged();
                    indicatorCube.SetActive(true);
	                break;
                case IndicatorType.LowerCivilizations:
	                LowerCivilizationChanged();
                    indicatorCube.SetActive(true);
                    break;
                case IndicatorType.Civilizations:
                    OwnerChanged();
                    indicatorCube.SetActive(true);
                    break;

            }
	    }
	}

    private void StarUpdate()
    {
        var mat = star.GetComponent<MeshRenderer>().material;
        switch (StarClass)
        {
            case StarClassType.O:
                mat.color = Color.blue;
                star.transform.localScale = new Vector3(35,35,35);
                break;
            case StarClassType.B:
                mat.color = new Color(51f/255f,153f/255f,1);
                star.transform.localScale = new Vector3(30, 30, 30);
                break;
            case StarClassType.A:
                mat.color = new Color(204f/255f, 229f/255f, 1);
                star.transform.localScale = new Vector3(25, 25, 25);
                break;
            case StarClassType.F:
                mat.color = Color.white;
                star.transform.localScale = new Vector3(20, 20, 20);
                break;
            case StarClassType.G:
                mat.color = Color.yellow;
                star.transform.localScale = new Vector3(15, 15, 15);
                break;
            case StarClassType.K:
                mat.color = new Color(1, 0.5f, 0);
                star.transform.localScale = new Vector3(10, 10, 10);
                break;
            case StarClassType.M:
                mat.color = Color.red;
                star.transform.localScale = new Vector3(5, 5, 5);
                break;
        }
        star.GetComponent<Light>().color = mat.color;
    }

    public enum StarClassType
    {
        O,
        B,
        A,
        F,
        G,
        K,
        M,
        Noone,
    }

    public enum IndicatorType
    {
        Noone,
        Gas,
        HeavyElements,
        LifeSupport,
        LowerCivilizations,
        Civilizations,
    }
}
